<?php
/* Smarty version 3.1.33, created on 2020-02-13 14:28:17
  from 'C:\wamp64\www\PrestaShop\themes\classic\templates\_partials\form-errors.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e45b1616779b9_56565696',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '347bcbecd83bb86f709f7bcbc27457b69f077ad2' => 
    array (
      0 => 'C:\\wamp64\\www\\PrestaShop\\themes\\classic\\templates\\_partials\\form-errors.tpl',
      1 => 1581621501,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e45b1616779b9_56565696 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if (count($_smarty_tpl->tpl_vars['errors']->value)) {?>
  <div class="help-block">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13513445945e45b161668cc8_18575702', 'form_errors');
?>

  </div>
<?php }
}
/* {block 'form_errors'} */
class Block_13513445945e45b161668cc8_18575702 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'form_errors' => 
  array (
    0 => 'Block_13513445945e45b161668cc8_18575702',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
?>
          <li class="alert alert-danger"><?php echo nl2br($_smarty_tpl->tpl_vars['error']->value);?>
</li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>
    <?php
}
}
/* {/block 'form_errors'} */
}
